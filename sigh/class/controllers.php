<?php
/**
 * Created by PhpStorm.
 * User: Noam
 * Date: 8/20/14
 * Time: 9:32 PM
 */

class UserController extends Controller {
    public function test() {
        echo 'panus';
    }
}

class IndexController extends Controller {

    public function index($params) {
        $blocks = array(
            "green" => array (
                "heading" => "News",
                "style" => "feature",
                "title" => "What's New?",
                "detail" => "Sigh reveals new Sigh.tv website!",
                "readmore" => "read more »",
                "image" => baseURL("img/Panel-Image.png"),
                "link" => "#",
            ),

            "blue" => array (
                "heading" => "Forums",
                "style" => "forum",
                "posts" => array(
                    /*
                    // Example "forum post"
                    array (
                        "title" => "Happy birthday to me!",
                        "author" => $user2->member_name,
                        "authorAvatar" => $user2->avatar,
                        "replier" => $user1->member_name,
                        "replierAvatar" => $user1->avatar,
                        "time" => $user2->birthdate,
                    ),*/
                ),
                "readmore" => "read more »",
                "link" => "#",
            ),

            "yellow" => array (
                "heading" => "Latest posts in the forums",
                "style" => "table",
                "table" => array (
                    "header" => array (
                        "series" => array (
                            "label" => "Series",
                        ),
                        "season" => array (
                            "label" => "Season",
                        ),
                    ),
                ),
                "readmore" => "browse our forums »",
                "link" => "#",
            ),

            "red" => array (
                "heading" => "Anime Countdown",
                "title" => "What's New?",
                "detail" => "Sigh reveals new Sigh.tv website!",
                "readmore" => "read more »",
                "link" => "#",
            ),
        );

        if (!empty($params["id"])) {
            $user = new UserModel($params["id"],$this->db);
        } else {
            $user = new UserModel(1,$this->db);
        }


        $header = array(
            "user" => array(
                "id" => $user->id,
                "online" => $user->online,
                "posts" => $user->posts,
                "avatar" => $user->avatar,
                "username" => $user->member_name,
            ),
        );

        $renderArray = array(
            "blocks" => $blocks,
            "header" => $header,
        );

        $this->render("home.html", $renderArray);
    }
}