<?php
// include and register Twig auto-loader
include 'vendor/autoload.php';
require_once("class/AltoRouter.php");
require_once("class/base.php");

$router = new AltoRouter();
$router->setBasePath(BASE_PATH);
$router->map('GET','/', array('c'=>'IndexController', 'a'=>'index'), 'index#index');
$router->map('GET','/fake/[i:id]', array('c'=>'IndexController', 'a'=>'index'), 'index#fake');
$match = $router->match();

if (!$match) {
    http_response_code(404);
    exit;
}

// specify where to look for templates
$loader = new Twig_Loader_Filesystem('templates');
// initialize Twig environment
$twig = new Twig_Environment($loader);


$twig->addFunction(new \Twig_SimpleFunction('asset', function ($asset) {
    return sprintf('http://'.SITE_BASE.BASE_PATH.'/%s', ltrim($asset, '/'));
}));


Twig_Autoloader::register();


$db = initDB();
$controllerType = $match["target"]['c'];
if (!class_exists($controllerType)) {
    http_response_code(404);
    exit;
}
$controller = new $controllerType($db,$twig);

if ($controller) {
    $action = $match["target"]["a"];
    if (!is_callable(array($controller,$action))) {
        http_response_code(500);
        exit;
    }
    $controller->$action($match["params"]);
}
