<?php
/**
 * Created by PhpStorm.
 * User: Noam
 * Date: 8/20/14
 * Time: 9:16 PM
 */

require_once("class.db.php");

if (!function_exists('http_response_code'))
{
    /**
     * Set/get HTTP response code.
     *
     * Sets the HTTP response code returned to the browser,
     * -or-
     * Gets the current code to be sent to the browser
     *
     * @link http://stackoverflow.com/questions/3258634/php-how-to-send-http-response-code
     *
     * @param int $newcode New HTTP response code
     * @return int Current HTTP response code
     */
    function http_response_code($newcode = NULL)
    {
        static $code = 200;
        if($newcode !== NULL)
        {
            header('X-PHP-Response-Code: '.$newcode, true, $newcode);
            if(!headers_sent())
                $code = $newcode;
        }
        return $code;
    }
}

define('BASE_PATH', '/zaitv');
define('SITE_BASE', 'www.agonize.me');
function baseURL($url) {
    $http = "http";
    if (isset($_SERVER['HTTPS'] )) {
        $http = "https";
    }
    return sprintf($http.'://'.SITE_BASE.BASE_PATH.'/%s', ltrim($url, '/'));
}

function initDB() {
    $mysqlHostname = "localhost";
    $mysqlUsername = "agoneaey_zaitv";
    $mysqlPassword = "K63LE99iy3Gz";
    $mysqlDatabase = "agoneaey_zaitv";
    //$mysqlPrefix = "smf";
    return new db("mysql:host=$mysqlHostname;dbname=$mysqlDatabase;charset=utf8", $mysqlUsername, $mysqlPassword);
}





class Controller {
    protected $db;
    protected $loader;
    protected $twig;

    public function __construct($db, $twig) {
        $this->db = $db;
        $this->twig = $twig;
    }
    protected function render($page, $renderArray) {
        // load template
        $template = $this->twig->loadTemplate($page);

        echo $template->render($renderArray);
    }
}

class Model {
    protected $data;
    protected $db;
    protected $table;
    protected $idfield;

    public function __construct($db, $table, $idf, $id) {
        $this->data = array();
        $this->table = $table;
        $this->idfield = $idf;
        $this->id = $id;
        $this->db = $db;
    }

    function __set($name, $value) {
        $this->data[$name] = $value;
    }
    function __get($name) {
        if ($this->data[$name]) {
            return $this->data[$name];
        }
        return NULL;
    }

    function reload($fields="*") {
        if ($this->db) {
            $data = $this->db->select($this->table, "{$this->idfield} = :id", array(":id"=>$this->id), $fields);

            if ($data) {
                foreach ($data as $field=>$value) {
                    $this->$field = $value;
                }
            }
        }
    }

    function save($fields=array()) {
        $fvArray = array();
        foreach ($fields as $fieldName) {
            $fvArray[$fieldName] = $this->$fieldName;
        }

        if (sizeof($fvArray) > 0) {
            $this->db->update($this->table, $fvArray, "{$this->idfield} = :id", array(":id"=>$this->id));
        }
    }
}

require_once("models.php");
require_once("controllers.php");