<?php

class UserModel extends Model {
    public function __construct($id, $db) {
        parent::__construct($db, "smf_members", "id_member", $id);
        $this->reload();
    }

    public function __get($name) {
        if ($name == "avatar" and empty($this->data["avatar"])) {
            return baseURL("img/missing_avatar.jpg");
        }
        return parent::__get($name);
    }
}